const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }

  let nums1 = str1 === '' ? [0] : [...str1].map(char => parseInt(char)).reverse();
  let nums2 = str2 === '' ? [0] : [...str2].map(char => parseInt(char)).reverse();

  if (nums1.some(value => isNaN(value)) || nums2.some(value => isNaN(value))) {
    return false;
  }

  if (nums2.length > nums1.length) {
    [nums1, nums2] = [nums2, nums1];
  }

  let carryOver = 0;
  for (let i = 0; i < nums1.length; i++) {
    nums1[i] += (nums2[i] || 0) + carryOver;
    carryOver = 0;

    if (nums1[i] > 9) {
      nums1[i] -= 10;
      carryOver = 1;
    }
  }

  if (carryOver > 0) {
    nums1.push(carryOver);
  }

  return nums1.reverse().join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = listOfPosts
    .filter(post => post.author === authorName)
    .length;
  let commentCount = listOfPosts
    .flatMap(post => post.comments)
    .filter(comment => comment && comment.author === authorName)
    .length;
  
  return `Post:${postCount},comments:${commentCount}`;
};

const tickets = (people) => {
  people = people.map(x => typeof x !== 'number' ? parseInt(x) : x);
  
  const bills = [100, 50, 25];
  const ticketPrice = 25;
  let cashRegister = {
    25: 0,
    50: 0,
    100: 0
  };

  for (const payed of people) {
    cashRegister[payed] += 1;
    let change = payed - ticketPrice;

    for (const bill of bills) {
      if (change >= bill) {
        let amount = Math.floor(change / bill);
        amount = Math.min(cashRegister[bill], amount);
        cashRegister[bill] -= amount;
        change -= amount * bill;
      }
    }

    if (change > 0) {
      return 'NO';
    }
  }

  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};